defmodule Rxjs.Repo.Migrations.CreateFilterLogs do
  use Ecto.Migration

  def change do
    create table(:filter_logs) do
      add :name, :string
      add :value, :integer

      timestamps()
    end

  end
end
