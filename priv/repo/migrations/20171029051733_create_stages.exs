defmodule Rxjs.Repo.Migrations.CreateStages do
  use Ecto.Migration

  def change do
    create table(:stages) do
      add :url, :string

      timestamps()
    end

  end
end
