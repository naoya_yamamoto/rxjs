defmodule Rxjs.Repo.Migrations.CreateMapLogs do
  use Ecto.Migration

  def change do
    create table(:map_logs) do
      add :name, :string
      add :value, :integer

      timestamps()
    end

  end
end
