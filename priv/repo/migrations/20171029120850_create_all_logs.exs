defmodule Rxjs.Repo.Migrations.CreateAllLogs do
  use Ecto.Migration

  def change do
    create table(:all_logs) do
      add :name, :string
      add :value, :integer

      timestamps()
    end

  end
end
