import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
@Component({
    selector: 'app-frame',
    templateUrl: './frame.component.html',
    styleUrls: ['./frame.component.css']
})
export class FrameComponent implements OnInit {

    private hrefs: { [key: string]: string } = {
        'filter': 'http://reactivex.io/documentation/operators/filter.html',
        'map': 'http://reactivex.io/documentation/operators/map.html',
        'delay': 'http://reactivex.io/documentation/operators/delay.html',
        'amb': 'http://reactivex.io/documentation/operators/amb.html',
        'concat': 'http://reactivex.io/documentation/operators/concat.html'
    };

    public href: SafeResourceUrl = '';

    constructor(
        private route: ActivatedRoute,
        private sanitizer: DomSanitizer
    ) { }

    ngOnInit() {
        this.route.params.subscribe(params => {
            this.href = this.sanitizer.bypassSecurityTrustResourceUrl(this.hrefs[params['param']]);
        });
    }

}
