import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs/Observable';
import { WebsocketService } from '../services/websocket.service';
@Component({
    selector: 'app-all',
    templateUrl: './all.component.html',
    styleUrls: ['./all.component.css']
})
export class AllComponent implements OnInit, OnDestroy {
    public log: any[] = [];

    public result: any = 0;

    public form: { name: string, value: number; } = { name: '', value: 0 };

    private room: string = 'all:lobby';

    public model: any = {
        skip: 0,
        take: 10,
        delay: 1000,
    };

    private sub: Subscription;
    private sub2: Subscription;
    constructor(
        private socket: WebsocketService
    ) { }

    ngOnDestroy() {
        if (this.sub) {
            this.sub.unsubscribe();
            this.sub2.unsubscribe();
            this.sub = undefined;
        }
    }

    ngOnInit() {
        this.socket.join(this.room);
        // this.socket.push(this.room, 'init', 'request');
        // init reset
        this.sub = this.socket.channel.filter(res => res.topic === this.room)
            .filter(res => this.initFilter(res))
            .subscribe(res => {
                this.result = 0;
                this.log = [];
                res.payload.log.forEach(item => {
                    this.logger(item);
                    this.calc(item);
                });
            });
        this.change();
    }

    private initFilter(res): boolean {
        return res.event === 'init' || res.event === 'reset';
    }

    submit(): void {
        let form = this.form;
        if (this.form.name === '') {
            form = Object.assign({}, form, { name: 'anonymous' });
        }
        this.socket.push(this.room, 'shout', form);
        this.form.value = 0;
    }

    logger(payload: { name: string, value: number }): void {
        this.log.unshift(payload);
    }

    calc(payload: { name: string, value: number }): void {
        this.result = this.result + payload.value;
    }

    private allFunction(obs: Observable<any>): Observable<any> {
        if (this.model.isOdd) {
            obs = obs.filter(res => {
                return res.payload.log.value % 2 === 1;
            });
        }
        if (this.model.isEven) {
            obs = obs.filter(res => res.payload.log.value % 2 === 0);
        }
        if (this.model.isNatural) {
            obs = obs.filter(res => res.payload.log.value > 0);
        }
        obs = obs.filter(res => this.log.length <= this.model.take);
        obs = obs.delay(this.model.delay);
        obs = obs.skip(this.model.skip);
        return obs;
    }

    change(): void {
        if (this.sub2 !== undefined) {
            this.sub2.unsubscribe();
            this.sub2 = undefined;
        }
        // main
        this.sub2 = this.socket.channel.filter(res => res.topic === this.room)
            .filter(res => !this.initFilter(res))
            .do(res => this.logger(res.payload.log))
            .let(obs => this.allFunction(obs))
            .subscribe(res => this.calc(res.payload.log)
        );
    }

    reset(): void {
        this.result = 0;
        this.log = [];
    }
}
