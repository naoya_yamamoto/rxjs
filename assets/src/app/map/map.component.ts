import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { WebsocketService } from '../services/websocket.service';
@Component({
    selector: 'app-map',
    templateUrl: './map.component.html',
    styleUrls: ['./map.component.css']
})
export class MapComponent implements OnInit, OnDestroy {

    public log: any[] = [];

    public result: any = 0;

    public form: { name: string, value: number; } = { name: '', value: 0 };

    private room: string = 'map:lobby';

    public ratio: number = 1;

    private sub: Subscription;
    private sub2: Subscription;
    constructor(
        private socket: WebsocketService
    ) { }

    ngOnDestroy() {
        if (this.sub) {
            this.sub.unsubscribe();
            this.sub2.unsubscribe();
            this.sub = undefined;
        }
    }

    ngOnInit() {
        this.socket.join(this.room);
        this.socket.push(this.room, 'init', 'request');
        // main
        this.sub = this.socket.channel.filter(res => res.topic === this.room)
            .filter(res => !this.initFilter(res))
            .do(res => this.logger(res.payload.log))
            .map(res => {
                res.payload.log.ratio = this.ratio;
                return res;
            })
            .subscribe(res => {
                this.result = this.result + (res.payload.log.value * res.payload.log.ratio);
            });

        // init
        this.sub2 = this.socket.channel.filter(res => res.topic === this.room)
            .filter(res => this.initFilter(res))
            .do(res => {
                this.result = 0;
                this.log = [];
                res.payload.log.forEach(item => {
                    this.logger(item);
                });
            })
            .subscribe(res => {
                res.payload.log.forEach(item => {
                    this.result = this.result + (item.value * item.ratio);
                });
            });
    }

    private initFilter(res): boolean {
        return res.event === 'init' || res.event === 'reset';
    }

    submit(): void {
        let form = this.form;
        if (this.form.name === '') {
            form = Object.assign({}, form, { name: 'anonymous' });
        }
        this.socket.push(this.room, 'shout', form);
        this.form.value = 0;
    }

    logger(payload: {name: string, value: number, ratio?: number }): void {
        payload.ratio = this.ratio;
        this.log.unshift(payload);
    }
}
