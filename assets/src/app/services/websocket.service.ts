import { Injectable } from '@angular/core';
import { PhoenixPayload, Payload } from 'phoenix-payload';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';
import 'rxjs/add/observable/dom/webSocket';
import 'rxjs/add/observable/interval';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/let';
import 'rxjs/add/operator/take';
import 'rxjs/add/operator/delay';
import 'rxjs/add/operator/skip';
import 'rxjs/add/operator/do';
import { environment } from '../../environments/environment';

@Injectable()
export class WebsocketService {

    // webSocket
    private subject = Observable.webSocket(PhoenixPayload.endPointURL('/socket/websocket', { token: 1234 }));

    // heartbeat
    private heartbeatSubject: Subscription;

    private responseSubject = new Subject<Payload<any>>();

    // response
    public channel = this.responseSubject.asObservable().filter(
        result => {
            // heartbeatをフィルター
            // phoenixの応答をフィルター
            return result.topic !== 'phoenix' &&
                result.event !== 'phx_error' &&
                result.event !== 'phx_reply' &&
                result.event !== 'phx_close';
        }
    ).do(console.log);

    public pageRoom: string = 'page:lobby';

    constructor() {
        this.subject.subscribe(
            (ret) => {
                this.responseSubject.next(PhoenixPayload.decode(ret));
            },
            (error) => {
                console.log(error);
                this.heartbeatSubject.unsubscribe();
                setTimeout(() => {
                    location.reload();
                }, 3000);
            },
            () => {
                console.log('complete');
            }
        );
        this.heartbeat();
        // ページ
        this.join(this.pageRoom);
        this.push(this.pageRoom, 'init', 'request');
    }

    public changePage(page: string): void {
        this.push(this.pageRoom, 'change', page);
    }

    private heartbeat(): void {
        this.heartbeatSubject = Observable.interval(30000).subscribe(
            () => {
                this.subject.next(PhoenixPayload.heartbeat());
            }
        );
    }

    public join(room: string): void {
        this.subject.next(PhoenixPayload.join(room));
    }

    public push(room: string, event: string, payload: any): void {
        this.subject.next(PhoenixPayload.push(room, event, payload));
    }
}
