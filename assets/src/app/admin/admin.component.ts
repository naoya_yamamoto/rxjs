import { Component, OnInit } from '@angular/core';

import { WebsocketService } from '../services/websocket.service';

@Component({
    selector: 'app-admin',
    templateUrl: './admin.component.html',
    styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {

    constructor(
        private socket: WebsocketService
    ) { }

    ngOnInit() {
        this.socket.join('map:lobby');
        this.socket.join('filter:lobby');
        this.socket.join('all:lobby');
    }

    top(): void {
        this.socket.changePage('top');
    }

    map(): void {
        this.socket.changePage('map');
    }

    map_reset(): void {
        this.socket.push('map:lobby', 'reset', 'reset');
    }

    filter(): void {
        this.socket.changePage('filter');
    }

    filter_reset(): void {
        this.socket.push('filter:lobby', 'reset', 'reset');
    }

    all(): void {
        this.socket.changePage('all');
    }

    all_reset(): void {
        this.socket.push('all:lobby', 'reset', 'reset');
    }

    frame(str: string): void {
        this.socket.changePage('frame/' + str);
    }

    end(): void {
        this.socket.changePage('end');
    }
}
