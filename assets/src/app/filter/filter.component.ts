import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { WebsocketService } from '../services/websocket.service';

@Component({
    selector: 'app-filter',
    templateUrl: './filter.component.html',
    styleUrls: ['./filter.component.css']
})
export class FilterComponent implements OnInit, OnDestroy {
    public log: any[] = [];

    public result: any = 0;

    public form: { name: string, value: number; } = { name: '', value: 0 };

    private room: string = 'filter:lobby';

    public ratio: number = 1;

    private sub: Subscription;
    private sub2: Subscription;
    constructor(
        private socket: WebsocketService
    ) { }

    ngOnDestroy() {
        if (this.sub) {
            this.sub.unsubscribe();
            this.sub2.unsubscribe();
            this.sub = undefined;
        }
    }

    ngOnInit() {
        this.socket.join(this.room);
        this.socket.push(this.room, 'init', 'request');
        // init reset
        this.sub = this.socket.channel.filter(res => res.topic === this.room)
            .filter(res => this.initFilter(res))
            .subscribe(res => {
                this.result = 0;
                this.log = [];
                res.payload.log.forEach(item => {
                    this.logger(item);
                    if (Math.abs(item.value) % 2 === this.ratio) {
                        this.calc(item);
                    }
                });
            }
            );
        // main
        this. sub2 = this.socket.channel.filter(res => res.topic === this.room)
            .filter(res => !this.initFilter(res))
            .do(res => this.logger(res.payload.log))
            .filter(res => Math.abs(res.payload.log.value) % 2 === this.ratio)
            .subscribe(res => this.calc(res.payload.log));
    }

    private initFilter(res): boolean {
        return res.event === 'init' || res.event === 'reset';
    }

    submit(): void {
        let form = this.form;
        if (this.form.name === '') {
            form = Object.assign({}, form, { name: 'anonymous' });
        }
        this.socket.push(this.room, 'shout', form);
        this.form.value = 0;
    }

    logger(payload: { name: string, value: number }): void {
        payload['ratio'] = this.ratio;
        this.log.unshift(payload);
    }

    calc(payload: { name: string, value: number }): void {
        this.result = this.result + payload.value;
    }
}
