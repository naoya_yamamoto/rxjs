import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';

import {WebsocketService} from './services/websocket.service';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

    constructor(
        private socket: WebsocketService,
        private router: Router
    ) { }

    ngOnInit() {
        this.socket.channel.filter(res => res.topic === this.socket.pageRoom).subscribe(
            res => {
                if (res.payload.page && location.href.indexOf('admin') === -1) {
                    this.router.navigate(['/' + res.payload.page]);
                }
            }
        );
    }
}
