import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Ng2BootstrapModule } from 'ngx-bootstrap';

@NgModule({
  imports: [
    CommonModule,
    Ng2BootstrapModule.forRoot()
  ],
  exports: [
      CommonModule,
      Ng2BootstrapModule,
      FormsModule
  ]
})
export class SharedModule { }
