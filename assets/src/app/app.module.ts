import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { SharedModule } from './shared/shared.module';
import { IndexComponent } from './index/index.component';
import { NavComponent } from './nav/nav.component';
import { FooterComponent } from './footer/footer.component';

import { WebsocketService } from './services/websocket.service';
import { AdminComponent } from './admin/admin.component';
import { MapComponent } from './map/map.component';
import { FilterComponent } from './filter/filter.component';
import { AllComponent } from './all/all.component';
import { FrameComponent } from './frame/frame.component';
import { EndComponent } from './end/end.component';

@NgModule({
    declarations: [
        AppComponent,
        IndexComponent,
        NavComponent,
        FooterComponent,
        AdminComponent,
        MapComponent,
        FilterComponent,
        AllComponent,
        FrameComponent,
        EndComponent,
    ],
    imports: [
        BrowserModule,
        RouterModule.forRoot([
            { path: 'admin', component: AdminComponent },
            { path: 'map', component: MapComponent },
            { path: 'filter', component: FilterComponent },
            { path: 'all', component: AllComponent },
            { path: 'frame/:param', component: FrameComponent },
            { path: 'end', component: EndComponent },
            { path: '**', component: IndexComponent }
        ]),
        SharedModule
    ],
    providers: [WebsocketService],
    bootstrap: [AppComponent]
})
export class AppModule { }
