#!/usr/bin/env bash
mix deps.get --only prod
MIX_ENV=prod mix compile
npm install ./assets --prefix ./assets
npm run build --prefix ./assets
MIX_ENV=prod mix phx.digest
MIX_ENV=prod mix ecto.migrate
kill `lsof -ti tcp:4100`
MIX_ENV=prod PORT=4100 elixir --detached -S mix do compile, phx.server
