use Mix.Config

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :rxjs, RxjsWeb.Endpoint,
  http: [port: 4001],
  server: false

# Print only warnings and errors during test
config :logger, level: :warn

# Configure your database
config :rxjs, Rxjs.Repo,
  adapter: Ecto.Adapters.MySQL,
  username: "root",
  password: "",
  database: "rxjs_test",
  hostname: "localhost",
  pool: Ecto.Adapters.SQL.Sandbox
