# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :rxjs,
  ecto_repos: [Rxjs.Repo]

# Configures the endpoint
config :rxjs, RxjsWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "5tUA6cSLYkaIS7A4E3eh4hBHRbreqOeSvW4bbncJpQo1RyBCwsOAUUCOzNHJli7D",
  render_errors: [view: RxjsWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: Rxjs.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"
