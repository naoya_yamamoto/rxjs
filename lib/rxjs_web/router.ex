defmodule RxjsWeb.Router do
  use RxjsWeb, :router


  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", RxjsWeb do
    pipe_through :browser # Use the default browser stack

    get "/robots.txt", PageController, :robot
    get "/*path", PageController, :index
    resources "/stages", StageController
  end

  # Other scopes may use custom stacks.
  # scope "/api", RxjsWeb do
  #   pipe_through :api
  # end
end
