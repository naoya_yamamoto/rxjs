defmodule RxjsWeb.StageController do
  use RxjsWeb, :controller

  alias Rxjs.Pages
  alias Rxjs.Pages.Stage

  def index(conn, _params) do
    stages = Pages.list_stages()
    render(conn, "index.html", stages: stages)
  end

  def new(conn, _params) do
    changeset = Pages.change_stage(%Stage{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"stage" => stage_params}) do
    case Pages.create_stage(stage_params) do
      {:ok, stage} ->
        conn
        |> put_flash(:info, "Stage created successfully.")
        |> redirect(to: stage_path(conn, :show, stage))
      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    stage = Pages.get_stage!(id)
    render(conn, "show.html", stage: stage)
  end

  def edit(conn, %{"id" => id}) do
    stage = Pages.get_stage!(id)
    changeset = Pages.change_stage(stage)
    render(conn, "edit.html", stage: stage, changeset: changeset)
  end

  def update(conn, %{"id" => id, "stage" => stage_params}) do
    stage = Pages.get_stage!(id)

    case Pages.update_stage(stage, stage_params) do
      {:ok, stage} ->
        conn
        |> put_flash(:info, "Stage updated successfully.")
        |> redirect(to: stage_path(conn, :show, stage))
      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", stage: stage, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    stage = Pages.get_stage!(id)
    {:ok, _stage} = Pages.delete_stage(stage)

    conn
    |> put_flash(:info, "Stage deleted successfully.")
    |> redirect(to: stage_path(conn, :index))
  end
end
