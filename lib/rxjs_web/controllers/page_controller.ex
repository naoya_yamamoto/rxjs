defmodule RxjsWeb.PageController do
  use RxjsWeb, :controller

  def index(conn, _params) do
    render conn, "index.html"
  end

  def robot(conn, _params) do
      text conn, ""
  end
end
