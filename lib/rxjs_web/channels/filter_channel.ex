defmodule RxjsWeb.FilterChannel do
  use RxjsWeb, :channel

  alias Rxjs.Pages

  def join("filter:lobby", payload, socket) do
    if authorized?(payload) do
      {:ok, socket}
    else
      {:error, %{reason: "unauthorized"}}
    end
  end

  # Channels can be used in a request/response fashion
  # by sending replies to requests from the client
  def handle_in("init", _payload, socket) do
    push socket, "init", %{log: Pages.list_filter_logs}
    {:noreply, socket}
  end

  # It is also common to receive messages from the client and
  # broadcast to everyone in the current topic (map:lobby).
  def handle_in("shout", payload, socket) do
    case Pages.create_filter_logs(payload) do
      {:ok, log} ->
          broadcast socket, "shout", %{log: Pages.log_json(log)}
          {:noreply, socket}
      {:error, %Ecto.Changeset{} = changeset} ->
          IO.inspect(changeset)
          {:noreply, socket}
    end
    {:noreply, socket}
  end

  def handle_in("reset", _payload, socket) do
    Pages.delete_filter_logs()
    broadcast socket, "reset", %{log: []}
    {:noreply, socket}
  end

  # Add authorization logic here as required.
  defp authorized?(_payload) do
    true
  end
end
