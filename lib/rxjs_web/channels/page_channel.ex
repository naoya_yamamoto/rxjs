defmodule RxjsWeb.PageChannel do
  use RxjsWeb, :channel

  alias Rxjs.Pages

  def join("page:lobby", payload, socket) do
    if authorized?(payload) do
      {:ok, socket}
    else
      {:error, %{reason: "unauthorized"}}
    end
  end

  # Channels can be used in a request/response fashion
  # by sending replies to requests from the client
  def handle_in("init", _payload, socket) do
    page = Pages.latest!
    push socket, "init", %{page: page}
    {:noreply, socket}
  end

  # It is also common to receive messages from the client and
  # broadcast to everyone in the current topic (page:lobby).
  def handle_in("change", payload, socket) do
    broadcast socket, "change", %{page: payload}
    case Pages.create_stage(%{url: payload}) do
      {:ok, _stage} ->
          {:noreply, socket}
      {:error, %Ecto.Changeset{} = _changeset} ->
          {:noreply, socket}
    end
  end

  # Add authorization logic here as required.
  defp authorized?(_payload) do
    true
  end
end
