defmodule Rxjs.Pages do
  @moduledoc """
  The Pages context.
  """

  import Ecto.Query, warn: false
  alias Rxjs.Repo

  alias Rxjs.Pages.Stage
  alias Rxjs.Pages.Map
  alias Rxjs.Pages.Filter
  alias Rxjs.Pages.All

  @doc """
  Returns the list of stages.

  ## Examples

      iex> list_stages()
      [%Stage{}, ...]

  """
  def list_stages do
    Repo.all(Stage)
  end

  @doc """
  Gets a single stage.

  Raises `Ecto.NoResultsError` if the Stage does not exist.

  ## Examples

      iex> get_stage!(123)
      %Stage{}

      iex> get_stage!(456)
      ** (Ecto.NoResultsError)

  """
  def get_stage!(id), do: Repo.get!(Stage, id)

  def latest! do
    query = from s in Stage,
      order_by: [desc: s.id],
      limit: 1,
      select: [s.url]
    result = Repo.all(query)
    _first(result)
  end

  defp _first([head|_tail]) do
      _first(head)
  end

  defp _first([]), do: ""

  defp _first(str) do
      str
  end

  @doc """
  Creates a stage.

  ## Examples

      iex> create_stage(%{field: value})
      {:ok, %Stage{}}

      iex> create_stage(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_stage(attrs \\ %{}) do
    %Stage{}
    |> Stage.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a stage.

  ## Examples

      iex> update_stage(stage, %{field: new_value})
      {:ok, %Stage{}}

      iex> update_stage(stage, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_stage(%Stage{} = stage, attrs) do
    stage
    |> Stage.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a Stage.

  ## Examples

      iex> delete_stage(stage)
      {:ok, %Stage{}}

      iex> delete_stage(stage)
      {:error, %Ecto.Changeset{}}

  """
  def delete_stage(%Stage{} = stage) do
    Repo.delete(stage)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking stage changes.

  ## Examples

      iex> change_stage(stage)
      %Ecto.Changeset{source: %Stage{}}

  """
  def change_stage(%Stage{} = stage) do
    Stage.changeset(stage, %{})
  end


  # マップのログ
  def list_map_logs do
      Repo.all(Map)
      |> Enum.map(fn arg -> log_json(arg) end)
  end

  def log_json(arg) do
      %{name: arg.name, value: arg.value, time: arg.inserted_at}
  end

  # マップのログ作成
  def create_map_logs(attrs \\ %{}) do
    %Map{}
    |> Map.changeset(attrs)
    |> Repo.insert()
  end

  def delete_map_logs do
      Repo.delete_all(Map)
  end

  # フィルターのログ
  def list_filter_logs do
      Repo.all(Filter)
      |> Enum.map(fn arg -> log_json(arg) end)
  end

  # フィルターのログ作成
  def create_filter_logs(attrs \\ %{}) do
    %Filter{}
    |> Filter.changeset(attrs)
    |> Repo.insert()
  end

  def delete_filter_logs do
      Repo.delete_all(Filter)
  end
  # 全てのログ
  def list_all_logs do
      Repo.all(All)
      |> Enum.map(fn arg -> log_json(arg) end)
  end

  # フィルターのログ作成
  def create_all_logs(attrs \\ %{}) do
    %All{}
    |> All.changeset(attrs)
    |> Repo.insert()
  end

  def delete_all_logs do
      Repo.delete_all(All)
  end
end
