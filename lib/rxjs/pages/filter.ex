defmodule Rxjs.Pages.Filter do
  use Ecto.Schema
  import Ecto.Changeset
  alias Rxjs.Pages.Filter


  schema "filter_logs" do
    field :name, :string
    field :value, :integer

    timestamps()
  end

  @doc false
  def changeset(%Filter{} = filter, attrs) do
    filter
    |> cast(attrs, [:name, :value])
    |> validate_required([:name, :value])
  end
end
