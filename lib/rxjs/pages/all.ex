defmodule Rxjs.Pages.All do
  use Ecto.Schema
  import Ecto.Changeset
  alias Rxjs.Pages.All


  schema "all_logs" do
    field :name, :string
    field :value, :integer

    timestamps()
  end

  @doc false
  def changeset(%All{} = all, attrs) do
    all
    |> cast(attrs, [:name, :value])
    |> validate_required([:name, :value])
  end
end
