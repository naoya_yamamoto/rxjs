defmodule Rxjs.Pages.Stage do
  use Ecto.Schema
  import Ecto.Changeset
  alias Rxjs.Pages.Stage


  schema "stages" do
    field :url, :string

    timestamps()
  end

  @doc false
  def changeset(%Stage{} = stage, attrs) do
    stage
    |> cast(attrs, [:url])
    |> validate_required([:url])
  end
end
