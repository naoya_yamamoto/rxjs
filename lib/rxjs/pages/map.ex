defmodule Rxjs.Pages.Map do
  use Ecto.Schema
  import Ecto.Changeset
  alias Rxjs.Pages.Map


  schema "map_logs" do
    field :name, :string
    field :value, :integer

    timestamps()
  end

  @doc false
  def changeset(%Map{} = map, attrs) do
    map
    |> cast(attrs, [:name, :value])
    |> validate_required([:name, :value])
  end
end
