defmodule Rxjs.WebSocket do
  use WebSockex
  require Logger

  def start_link(url, :state) do
    WebSockex.start_link(url, __MODULE__, :state)
  end

  def handle_frame({:text, msg}, :state) do
      Logger.info(msg)
      IO.puts "Received Message - Type: -- Message: #{inspect msg}"
      {:ok, :state}
  end
  def handle_info({:text, msg}, :state) do
      Logger.info(msg)
      IO.puts "Received Message - Type: -- Message: #{inspect msg}"
      {:ok, :state}
  end

  def echo(pid, message) do
      WebSockex.send_frame(pid, {:text, message})
  end

  def handle_connect(_conn, :state) do
      Logger.info("Connected!")
      {:ok, :state}
  end
  def handle_disconnect(%{reason: {:local, reason}}, :state) do
      Logger.info("Local close with reason #{inspect reason}")
      {:ok, :state}
  end
end
